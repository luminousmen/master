##### Summary

[1] Efficient QoS for Multi-Tiered Storage Systems (hotstorage12-final)

This  paper  raises  issues  that  arise  in  providing  QoS in  multi-tiered  storage  architectures,   and  presents  a resource  allocation  model  and  scheduling  framework suited for this situation.

QoS  performance  model  called reward allocation
Reward allocation model [8, 9] emulates a client’s performance profile when running on a dedicated infrastructure.
