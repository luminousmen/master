#include <map>
#include <ctime>
#include <string>
#include <vector>
#include <sstream>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <algorithm>


/* block = 512 bytes */
const double BLOCK_SIZE = 512;

struct Block{
    int             app_id;
    long double     block_number;
    int             storage_type;
    long double     speed;
    unsigned int    frequency;
};

struct Storage_type{
    std::string     name;
    long long int   size;
    float           speed;
    int             id;    
};


template <typename T1, typename T2>
struct less_second {
    typedef std::pair<T1, T2> type;
    bool operator ()(type const& a, type const& b) const {
        return a.second.frequency > b.second.frequency;
    }
};

std::vector<std::string> split(const std::string &s, char delim);

float string_to_float(std::string thestring);

std::map<std::string, Block> repartition(
    std::map<std::string, Block> storage,
    Storage_type storages[]);

int choose_storage();

double calculate_response_time(Block b);

Block change_storage_type(std::string, Block request, Storage_type storages[]);

double get_or_create_blocks_from_request(
    std::map<std::string, Block> &storage_blocks,
    std::vector<std::string> request,
    Storage_type storages[]);

void show_storage(std::map<std::string, Block> storage);
