#include "header.h"

#define HOUR 60 * 60 * 1
#define DEBUG false



Storage_type storages[3] = {
            { "SSD",    2000000,        500000,     0 },
            { "HDD1",   80000000,       150000,     1 },
            { "HDD2",   10000000000,    1000,       2 }
        };



int main(int argc, char * argv[]){
    
    char * TRACEFILE = "/home/kir/pROJECTS/master/program/Financial1.spc";
    
    if( DEBUG ){
        TRACEFILE = "/home/kir/pROJECTS/master/program/short.spc";
    }

    
    std::ifstream trace;
    trace.open(TRACEFILE);

    if ( !trace.is_open() ){
        std::cout << "Can't open trace file" << std::endl;
        return 0;
    }

    std::string tmp;
    unsigned int request_counter = 0;
    double avg_response_time = 0;
    /* time from request */
    long double system_time = 0;
    long double all_time = 0;
    int repartision_counter = 0;

    std::map<std::string, Block> storage;
    
    std::cout << "Starting trace...\n\n";

    /*
     * 
     * MAIN CYCLE
     * 
     * */
    clock_t begin = clock();
    while( !trace.eof() ){

        /* once in an hour repartition */
        if ( system_time > HOUR ){
            storage = repartition(storage, storages);
            if( DEBUG ){
                std::cout << "Current system time is " << system_time << std::endl;
                std::cout << "REPARTITION" << std::endl;
            }
            all_time += system_time;
            system_time = 0;
            repartision_counter++;
        }
        
        trace >> tmp;
        
        /* splitting incoming request */
        std::vector<std::string> request = split(tmp, ',');
        
        /* base finding-retriving-changing blocks */
        avg_response_time += get_or_create_blocks_from_request(storage, request, storages);

        /* count all requests */
        ++request_counter;
        system_time = string_to_float( request[4] ) - all_time;
        
        if( DEBUG ){
            std::cout << "Current system time is " << system_time << std::endl;
            show_storage(storage);
        }
    }
    /*
     * END MAIN CYCLE
     * */
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

    trace.close();
    
    avg_response_time /= request_counter;
    if( DEBUG ){
        std::cout << "System time: " << system_time << std::endl;
    }
    
    std::cout << "***********" << std::endl;
    std::cout << "Done!" << std::endl;
    std::cout << "Elapsed time " << elapsed_secs << " sec" << std::endl;
    std::cout << "***********" << std::endl;

    std::cout << "Num of requests: " << request_counter << std::endl;
    std::cout << "Avg: " << avg_response_time << " sec" << std::endl;
    std::cout << "Repartision: " << repartision_counter << " times" << std::endl;
    std::cout << "End time: " << all_time << std::endl;


    
    return 0;
}
