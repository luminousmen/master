#include "header.h"

/* round robin cycle for choosing storage */
unsigned int last_state = 1;

/**
 * @brief Method of live recalculated average
 * @param avg Current calculated average
 * @param new_sample New value
 * @param counter Number of values
 * @return New average value.
 */
double update_average(double avg, double new_sample, unsigned int counter) {
    avg -= avg / counter;
    avg += new_sample / counter;
    return avg;
}


/**
 * @brief Converts string to float
 * @param str The input string which must converted to float
 * @return Float value.
*/
float string_to_float(std::string str) {
    float value;
    std::istringstream ss(str);
    ss >> value;
    return value;
}

/**
 * @brief splitting string
 * @param s
 * @param delim
 * @return tokens
 */
std::vector<std::string> split(const std::string &s, char delim) {
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> tokens;

    while (getline(ss, item, delim)){
        tokens.push_back(item);
    }

    return tokens;
}


/**
 * @brief Sorting requests in storage by frequency
 * @param storage
 * @return sorted vector of pairs
 */
std::vector<std::pair<std::string, Block> > sort_blocks(
    std::map<std::string, Block> storage) {
    
    std::vector<std::pair<std::string, Block> > sorted_blocks(storage.begin(), storage.end());
    /* sort blocks by frequency */
    std::sort(sorted_blocks.begin(), 
        sorted_blocks.end(), 
        less_second<std::string, Block>()
    );
    
    return sorted_blocks;
}

/**
 * @brief Change storage during repartision
 * Zeroing all blocks frequencies (!)
 * @param b
 * @param request
 * @param storages
 * @return Block with new values
 */
Block change_storage_type(std::string block_num, Block b, Storage_type storages[]) {
    /* decreasing storage speed */
    for(int i = 0; i < 3 ; i++) {
        /* check if storage have enough space for new block OR 
         * block already in this storage
         * */
        if(BLOCK_SIZE < storages[i].size || b.storage_type == storages[i].id){
            storages[i].size -= BLOCK_SIZE;
            storages[b.storage_type].size += BLOCK_SIZE;
            b.storage_type = storages[i].id;
            b.speed = storages[i].speed;
            b.frequency = 0; // zeroing frequency
            //std::cout << "For item:" << b << ";chosen:" << storages[i].name << "-" << storages[i].id << std::endl;
            return b;
        }
    }
    std::cout << "Out of memory!" << std::endl;
    return b;
}


/**
* @brief Main repartition function
* @param storage
* @param storages All storages in system
*/
std::map<std::string, Block> repartition(
    std::map<std::string, Block> storage, 
    Storage_type storages[]) {
    
    auto sorted_blocks = sort_blocks(storage);
    
    /* iterate through sorted vector */
    for(auto it = begin(sorted_blocks); it != end(sorted_blocks); ++it) {
        
        std::map<std::string, Block>::iterator found;
        found = storage.find( it->first );
        
        it->second = change_storage_type(found->first, found->second, storages);
    }
    
    /* 
     * copy from vector to map back 
     * */
     std::map<std::string, Block> new_storage;
     for(auto it = begin(sorted_blocks); it != end(sorted_blocks); ++it) {
            new_storage[it->first] = it->second;
    }
    
    return new_storage;
}


/**
* @brief Choosing storage for current input value
* @return Choosed storage number.
*/
int choose_storage() {
    return ++last_state % 3;
}

/**
 * @brief 
 * @param b - current block 
 * @return response time
 */
double calculate_response_time(Block b) {
    return BLOCK_SIZE / b.speed;
}

/**
 * @brief 
 * @param request
 * @param block_num
 * @param storages
 * @return new Block instance
 */
Block create_new_block(
    std::vector<std::string> request,
    long int block_num,
    Storage_type storages[]) {
        
    Block b;
    b.app_id = string_to_float(request[0]);
    b.block_number = block_num;
    b.frequency = 1;
    b.storage_type = choose_storage();
    b.speed = storages[b.storage_type].speed; // bytes per sec
    return b;
}

/**
 * @brief This function implements basic iteration per request.
 * It searches stotage for request blocks. Iteration parameter is global variable BLOCK_SIZE
 * @param storage_blocks
 * @param request - vector of strings (result of splitting string of trace file)
 * @param storages
 * @return average response time for current request
 */
double get_or_create_blocks_from_request(
    std::map<std::string, Block> &storage_blocks,
    std::vector<std::string> request,
    Storage_type storages[]) {
    
    int begin_block = string_to_float(request[1]);
    int end_block = begin_block + string_to_float(request[2]);
    double avg_response_time = 0;
    
    std::map<std::string, Block>::iterator found_block;
    Block b;
    
    /* start from starting block number in request. Incerementing by knowing block size */
    for(int block_num = begin_block; block_num < end_block; block_num += BLOCK_SIZE){
        
        //std::cout << "BLOCK NUM>" << block_num << std::endl;
        
        /* searching by block_number */
        if( (found_block = storage_blocks.find(std::to_string(block_num)) ) != storage_blocks.end() ){
            /*
             * FOUND
             * */
            b = found_block->second;
            b.frequency++;
            found_block->second = b;
            //show_storage(storage_blocks);
        }
        else {
            /*
             * NOT FOUND
             * */
            b = create_new_block(request, block_num, storages);
            /* add block to storage and store block Info */
            storage_blocks[std::to_string(block_num)] = b;
            //show_storage(storage_blocks);
        }
        
        /* avg = speed * size */
        avg_response_time += calculate_response_time(b);
    }
    
    return avg_response_time;
}


/* DEBUG FUNCTIONS */
void show_storage(std::map<std::string, Block> storage){
    for(auto it = begin(storage); it != end(storage); ++it) {
        std::cout << it->first << " " << "storage:" << it->second.storage_type << ";frequency:" << it->second.frequency << "\n";
    }
}