##### Traces
http://traces.cs.umass.edu/index.php/Storage/Storage

##### SPC TRACE FILE FORMAT SPECIFICATION:
1,10356592,4096,W,2.449733

ASU(Logical block address),block,data_size(in bytes),operation,sys_time(not difference) in sec

```
file    FS     block    SCSI
-----> [   ] -------->  [  ]
                        [  ]
```


Measure QoS in IOPS

```
bytes_in_second = IOPS * size_of_block_in_bytes
```

##### TODOs

* byte_size / speed is known value(!)
* characterize storage by response time per BYTE
* optimize code by performance
* try reallocate memory by hand
* find new articles/info about QoS providing
* write letters to researchers


##### Problems to solve

* knapsack problem?!
* storage resource allocation problem
* noisy neighbors problem

Multi-tiered storage system

Using multiple tiers provides a flexible trade-off in terms of IOPS performance and storage capacity. And also it saves money. But providing QoS guarantees for clients is a difficult thing.

 Storage quality of service (QoS) is critical for companies and service providers who want to deliver consistent primary storage performance to business-critical applications. For many applications, raw performance is not the major challenge. It’s often much more important to ensure that performance is both predictable and guaranteed. This is virtually impossible in traditional hybrid or disk-based storage infrastructures, no matter how advanced the chosen QoS method might claim to be.

Many popular QoS methods used in storage today are simply inadequate. They address common operational functions like tiering, rate limiting, prioritization, hypervisor-based QoS, and caching.

##### Links

EMC FAST: http://www.emc.com/corporate/glossary/fully-automated-storage-tiering.htm

EMC fully automated storage tiering (FAST) automatically moves active data to
high-performance storage tiers and inactive data to low-cost, high-capacity
storage tiers. The result is higher performance, lower costs, and a denser
footprint than conventional systems. With FAST, enterprise flash drives
increase application performance by up to 800 percent, and serial advanced
technology attachment (SATA) disk drives lower costs by up to 80 percent.
